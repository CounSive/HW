/* 
Теоретичне питання
Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
Для чого потрібно викликати super() у конструкторі класу-нащадка?

1. Прототипне наслідування в JavaScript - це механізм, 
за допомогою якого об'єкти можуть успадковувати властивості і методи від інших об'єктів
2. У JavaScript ключове слово super() використовується у конструкторі класу-нащадка для виклику конструктора класу-батька. 
Це потрібно, коли клас-нащадок має свій власний конструктор, 
але в той же час він також хоче виконати конструктор класу-батька для успадкованих властивостей та ініціалізації.



Завдання
Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). 
Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль. 
*/


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    // Геттери та сеттери для властивостей
    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    // Перезаписаний гетер для властивості salary
    get salary() {
        return super.salary * 3; // Перезаписаний гетер повертає salary, помножену на 3
    }

    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }
}

// Створення екземплярів класу Programmer
const programmer1 = new Programmer('John', 30, 5000, 'JavaScript', 'Python');
const programmer2 = new Programmer('Alice', 25, 4500, 'Java', 'C++');

// Виведення екземплярів у консоль
console.log(programmer1);
console.log(programmer2);
console.log(programmer1.salary);