/* 
Теоретичне питання
Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

У JavaScript асинхронність означає, що код може виконуватися не послідовно, а здебільшого у фоновому режимі, без очікування завершення певних операцій перед продовженням виконання наступного коду. 
Це дозволяє веб-додаткам виконувати завдання, які займають багато часу, такі як завантаження даних з сервера чи робота з файлами, не блокуючи основний потік виконання коду. 
Замість цього, коли виконання асинхронного коду завершується, викликається колбек або promise, щоб повідомити про завершення операції та обробити отримані дані або виконати інші дії. 
Це забезпечує кращу реактивність веб-додатків і уникнення блокування інтерфейсу користувача під час виконання довгих операцій.

------------------------------------------------------------------------------------------------------------------------------------------------------

Завдання
Написати програму "Я тебе знайду по IP"

Технічні вимоги:
Створити просту HTML-сторінку з кнопкою Знайти по IP.
Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
Усі запити на сервер необхідно виконати за допомогою async await.
*/

const btn = document.querySelector(".btn-find-ip");
const infoContainer = document.querySelector(".info-container");
const countrySpan = document.querySelector(".info-container__country");
const regionSpan = document.querySelector(".info-container__region");
const citySpan = document.querySelector(".info-container__city");

const getInfoByIp = (countrySpan, regionSpan, citySpan, country, region, city) => {
    countrySpan.textContent = `Conutry: ${country}`;
    regionSpan.textContent = `Region: ${region}`;
    citySpan.textContent = `City: ${city}`;
};

const sendRequest = async () => {
    try {
        const ipRequest = await fetch("https://api.ipify.org/?format=json");
        const { ip } = await ipRequest.json()
        const infoRequest = await fetch(`http://ip-api.com/json/${ip}`);
        console.log(infoRequest);
        const { city, country, regionName } = await infoRequest.json();


        getInfoByIp(countrySpan, regionSpan, citySpan, country, regionName, city);

    } catch (err) {
        console.log(err);
    }
}

btn.addEventListener("click", sendRequest)


