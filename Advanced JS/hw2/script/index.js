/* Теоретичне питання
Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
 1. Читання файлів
 2.Робота з мережевими запитами
 3.Операції з базою даних
 4.Парсинг даних
 5.Використання зовнішніх бібліотек

Завдання */

/* 
Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці. */
const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const cardContainer = document.querySelector("#root");


class BooksAuthorError extends Error {
    constructor(books) {
        super(`Author name is not dafined in: ${JSON.stringify(books)}`);
        this.name = "AutorNameError"
    }
}

class BooksNameError extends Error {
    constructor(books) {
        super(`Book name is not dafined in: ${JSON.stringify(books)}`);
        this.name = "BookNameError"
    }
}

class BooksPriceError extends Error {
    constructor(books) {
        super(`Book price is not dafined in: ${JSON.stringify(books)}`);
        this.name = "BookPriceError"
    }
}



class BooksCard {
    constructor(books) {
        if (!('author' in books)) {
            throw new BooksAuthorError(books);
        } else if (!('name' in books)) {
            throw new BooksNameError(books);
        } else if (!('price' in books)) {
            throw new BooksPriceError(books);
        }
        this.author = books.author; // Отримуємо автора з об'єкта books
        this.name = books.name; // Отримуємо назву книги з об'єкта books
        this.price = books.price; // Отримуємо ціну книги з об'єкта books
    }

    render(container) {
        container.insertAdjacentHTML("beforeend", `<ul>
            <li>${this.author ? this.author + ' - ' : ''}${this.name} - ${this.price}$</li>
        </ul>`);
    };
}

books.forEach(book => {
    try {
        new BooksCard(book).render(cardContainer)

    } catch (error) {
        if (error.name === 'AutorNameError') {
            console.warn(error);
        } else if (error.name === 'BookNameError') {
            console.warn(error);
        } else if (error.name === 'BookPriceError') {
            console.warn(error);
        } else {
            throw error;
        }


    }
});



