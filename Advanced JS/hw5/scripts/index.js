/* 
Завдання
Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
Технічні вимоги:
При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
https://ajax.test-danit.com/api/json/users
https://ajax.test-danit.com/api/json/posts
Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
Кожна публікація має бути відображена у вигляді картки (приклад доданий в репозиторії групи, посилання на ДЗ вище), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.
*/

const POSTS_URL = "https://ajax.test-danit.com/api/json/posts"
const USERS_URL = "https://ajax.test-danit.com/api/json/users"

class Card {
    constructor(name, email, title, text, id) {
        this.name = name;
        this.email = email;
        this.title = title
        this.text = text;
        this.id = id
    }

    container = document.createElement("div");
    btnContainer = document.createElement("div");
    deleteBtn = document.createElement("button");

    deletHandler() {
        this.deleteBtn.addEventListener('click', () => {
            axios.delete(`${POSTS_URL}/${this.id}`)
                .then(({ status }) => {
                    if (status === 200) {
                        this.container.remove()
                    }
                })
                .catch(err => {
                    throw new Error(err)
                })
        })
    }

    render() {
        this.container.insertAdjacentHTML("beforeend",
            `<div class="user-info-container">
                <span>${this.name}</span>
                <span>${this.email}</span>
            </div>
                <h2>${this.title}</h2>
                <p>${this.text}</p>`
        );
        this.container.classList.add = "post-wrapper"
        this.btnContainer.className = "btn-wrapper";
        this.deleteBtn.innerText = "Delete";
        this.deleteBtn.classList.add = "delete-btn";
        this.btnContainer.append(this.deleteBtn);
        this.container.append(this.btnContainer);
        document.querySelector(".posts-container").append(this.container);

        this.deletHandler();
    }
}

axios.get(USERS_URL)
    .then(({data}) => {
        const userArrey = data.map(({id, name, email}) => {
            const userData = {id, name, email};
            return userData;
        })

        axios.get(POSTS_URL)
            .then(({data: post}) => {
                userArrey.forEach(({id, name, email}) => {
                    post.forEach(({id: postId, userId, title, body}) => {
                        if(id === userId) {
                            new Card(name, email, title, body, postId).render();
                        }
                    })
                })
            })
            .catch(err => {
                throw new Error(err)
            })
    })
    .catch(err => {
        throw new Error(err)
    })




