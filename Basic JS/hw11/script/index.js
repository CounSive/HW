/* 
Теоретичні питання
1. Що таке події в JavaScript і для чого вони використовуються?
2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?

Практичні завдання
 1. Додати новий абзац по кліку на кнопку:
  По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
 
 2. Додати новий елемент форми із атрибутами:
 Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
    По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
 */

/* Теоретичні питання 
1. В JavaScript події (events) - це важливий механізм, який дозволяє вам реагувати на різноманітні дії користувачів або на зміни у середовищі виконання програми. 
Події виникають в результаті взаємодії користувача з веб-сторінкою, такою як натискання кнопок миші, клавіш клавіатури, завантаження сторінки, або зміни розміру вікна

2. 2.1. click Виникає при натисканні лівої кнопки миші на елементі.
    2.2 dblclick Виникає при подвійному натисканні лівої кнопки миші на елементі.
    2.3. mousedown Виникає при натисканні будь-якої кнопки миші на елементі.
    2.4. mouseup Виникає після відпускання будь-якої кнопки миші на елементі.
    2.5. mousemove Виникає при русі миші над елементом.
    2.6. mouseover
    2.7. mouseout
    2.8. contextmenu 

3. Подія "contextmenu" виникає, коли користувач намагається викликати контекстне меню на елементі. 
    Зазвичай це відбувається, коли користувач правоклікає на елемент мишею. 
    Подія "contextmenu" дозволяє вам визначити власні дії або відмовити виклику стандартного контекстного меню браузера.

    Практичні завдання
*/


// 1 заввдання 

const section = document.querySelector("#content");

const p = document.querySelector("p")
const btn = document.querySelector("#btn-click");

btn.addEventListener("click", () => {
    p.insertAdjacentHTML(`afterend`, `<p>New Paragraph</p>`);
});


// 2 завдання 
const footer = document.querySelector("footer");

const indputBtn = document.createElement("button")

indputBtn.id = "btn-input-create"
indputBtn.innerText = "Add input"

footer.before(indputBtn)

indputBtn.addEventListener("click", () => {
    footer.insertAdjacentHTML(`beforebegin`, `<input type="text" name="name" placeholder="Enter your name">`)
});



