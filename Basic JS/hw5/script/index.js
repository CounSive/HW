/* Теоретичні питання
1. Як можна сторити функцію та як ми можемо її викликати?
2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
4. Як передати функцію аргументом в іншу функцію? 

1.  Існує кілька способів визначення функцій. Найбільш поширені - function declaration, function expression.

    1. Function declaration
    Function declaration (Оголошення функцій) визначає ім'я функції за допомогою ключового слова `function`. Зазвичай вони піднімаються (hoisted),що означає, що вони доступні в усій області, в якій вони визначені, навіть якщо вони оголошені після їх використання.
    
    function functionName(params) {
        Function body
        Code to be executed

        return value; // Optional, returns a value
    }

    2. Function Expression
    Function Expression передбачає визначення функції та присвоювання її змінній. Ці функції можуть бути анонімними або мати ім'я. Вони не піднімаються (не доступні до виклику) і можуть бути використані лише після того, як їх призначено.

    const functionName = function(params) {
        Function body
        Code to be executed

        return value; // Optional, returns a value
    }

    Щоб викликати функцію нам потрібно використати 
    // Виклик функціі без параметрів
    functionName();

    // Виклик функціі з параметрами
    functionName(5, 3);

2. Оператор повернення(return): Використовується для завершення виконання функції та вказує значення, яке повертається викликачеві.

    Щоб використати return, треба написати return та значення яке ми хочемо повернути. 

3. У контексті функцій терміни "параметри" та "аргументи" використовуються для опису змінних, які використовуються у функції:

    1. Параметри:

    Параметри - це змінні, які вказуються у визначенні функції. Вони служать як місце, де функція отримує дані для обробки.
    Параметри знаходяться у дужках під час визначення функції.
    Функція може мати жодного або більше параметрів.

    2. Аргументи:

    Аргументи - це конкретні значення, які передаються функції під час її виклику.
    Вони підставляються у місця параметрів у функції під час виклику.
    Функція отримує та обробляє аргументи для виконання певних дій.

4. У JavaScript функції є об'єктами першого класу, що означає, що їх можна передавати як аргументи іншим функціям. Це називається передачею функції як аргументу. Для цього використовується ім'я функції без виклику, тобто без використання круглих дужок.
*/

/* Практичні завдання
1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.
2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
 */

//1. Завдання  1

/* const divideNumbers = (firstNumber, secondNumber) => {
    do {
        let firstUserNumber = +prompt("Введіть перше число:");
        if (isNaN(firstUserNumber)) {
            alert("Будь ласка, введіть коректне число.");
        } else {
            firstNumber = firstUserNumber;
        }
    } while (isNaN(firstNumber));

    
    do {
        let secondUserNumber = +prompt("Введіть друге число:");
        if (isNaN(secondUserNumber)) {
            alert("Будь ласка, введіть коректне число.");
        } else if (secondUserNumber !== 0){
            secondNumber = secondUserNumber;
        } else {
            alert("Ділення на нуль неможливе")
        }
    } while (isNaN(secondNumber) && secondNumber !==0);

    return firstNumber / secondNumber
}

const result = divideNumbers()
console.log(result);
 */

//2. Завдання 2

const getUserNumber = (promptMessage) => {
    
    let userInput;
    let userNumber;
    do {
        userInput = +prompt(promptMessage);
        if (isNaN(userInput)) {
            alert("Будь ласка, введіть коректне число.");
        } else {
            userNumber = userInput;
        }
        
    } while (isNaN(userNumber));

    return userNumber;
}

const getUserOperation = () => {
    let userOperation;
  
    do {
      userOperation = prompt("Введіть математичну операцію (+, -, *, /):");
      if (userOperation !== "+" && userOperation !== "-" && userOperation !== "*" && userOperation !== "/") {
        alert("Такої операції не існує")
      }
    } while (!["+", "-", "*", "/"].includes(userOperation));
  
    return userOperation;
  }

const performOperation = (a, b, operation) => {
    switch (operation) {
      case '+':
        return a + b;
      case '-':
        return a - b;
      case '*':
        return a * b;
      case '/':
        // Перевірка ділення на нуль
        if (b !== 0) {
          return a / b;
        } else {
          alert("Помилка: Ділення на нуль неможливе");
          return undefined;
        }
      default:
        return undefined;
    }
  }

const number1 = getUserNumber("Введіть перше число:");
const number2 = getUserNumber("Введіть друге число:");

// Отримання математичної операції від користувача
const operation = getUserOperation();

// Виконання математичної операції та виведення результату в консоль
const result = performOperation(number1, number2, operation);
if (result !== undefined) {
  console.log(`Результат виконання операції ${number1} ${operation} ${number2} дорівнює: ${result}`);
}