/* 
Теоретичні питання
1. В чому відмінність між setInterval та setTimeout?
2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

Практичне завдання:
-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.
Практичне завдання 2:
Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".


1. 1.1 Інтервали часу:
    setInterval: Виконує передану функцію через певний проміжок часу, який визначається другим параметром методу. Цей проміжок вказується в мілісекундах.
    setTimeout: Виконує передану функцію один раз через певний проміжок часу, який також визначається другим параметром. Після цього виконання завдання припиняється.

    1.2 Повторення виконання:
    setInterval: Виконання функції буде повторюватися регулярно з інтервалом, який ви вказали, до тих пір, поки ви не викличете clearInterval.
    setTimeout: Виконання функції відбувається один раз через вказаний часовий інтервал, і після цього завдання припиняється. Щоб здійснити повторення, вам доведеться використовувати рекурсивний виклик setTimeout.

    1.3 Відкладене виконання:
    setInterval: Виконання функції розпочинається через певний часовий інтервал після виклику setInterval.
    setTimeout: Виконання функції розпочинається через певний часовий інтервал після виклику setTimeout.

    1.4 Параметри:
    setInterval: Приймає два параметри: функцію для виконання та інтервал часу в мілісекундах між викликами функції.
    setTimeout: Приймає два параметри: функцію для виконання та час в мілісекундах, через який функція повинна бути викликана.

2. Для припинення виконання функції, яка була запланована для виклику з використанням setTimeout або setInterval, ви можете використовувати метод clearTimeout або clearInterval відповідно.
*/

const timeOut = document.querySelector(".time_out");
const timeOutBtn = document.querySelector(".time_out-btn");
const counter = document.querySelector(".counter");
let count = 10;



timeOutBtn.addEventListener('click', () => {
    setTimeout(() => {
        timeOut.innerText = "Succses";
    }, 3000);
});

window.addEventListener('load', () => {
    const countInterval = setInterval(() => {
        count--;

        counter.innerText = count;

        if (count === 0) {
            counter.innerText = "Зворотній відлік завершено";

            clearInterval(countInterval)
        }
    }, 1000);
});
