/* 
1. В чому полягає відмінність localStorage і sessionStorage?
2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?

1. 1.1 Тривалість збереження:
    localStorage: Дані в localStorage залишаються збереженими на протязі нескінченно довгого часу, навіть після закриття браузера та перезавантаження комп'ютера.
    sessionStorage: Дані в sessionStorage зберігаються тільки протягом часу життя одного сеансу браузера. Якщо ви закриєте вкладку чи вікно браузера, дані з sessionStorage будуть видалені.
    
    1.2 Об'єм зберігання:
    Обидва об'єкти мають максимальний об'єм зберігання даних, який зазвичай становить 5-10 МБ для кожного домену.
    
    1.3 Доступність даних:
    localStorage: Дані в localStorage доступні для всіх вкладок та вікон браузера з тим же самим доменом.
    sessionStorage: Дані в sessionStorage доступні тільки для вкладок/вікон, в яких вони були створені.

2. 2.1. Не зберігати чутливі дані
    2.2. Шифрування даних
    2.3. Обмежте обсяг зберіганої інформації:
    2.4. Використання безпеки проти Cross-Site Scripting (XSS)
    2.5. Застосування безпеки проти Cross-Site Request Forgery (CSRF)
    2.6. Завжди використовуйте HTTPS
    2.7. Оновлюйте програмне забезпечення

3. Коли завершується сеанс браузера, дані, збережені в sessionStorage, автоматично видаляються. 
    sessionStorage призначений для тимчасового зберігання даних, і його область видимості обмежена часом життя сесії браузера.

Практичне завдання:
Реалізувати можливість зміни колірної теми користувача.
Технічні вимоги:
- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.
*/

const themeBtn = document.querySelector(".theme-togle-btn");
let theme = 'light';

window.addEventListener("load", (event) => {
    event.preventDefault()
    theme = localStorage.getItem('theme');
    if(theme === null) {
        changeTheme('light')
    }else {
        changeTheme(theme)
    }
});

themeBtn.addEventListener("click", (event) => {
    event.preventDefault();
    if (theme === "light") {
        changeTheme('dark')
    } else {
        changeTheme('light')
    }
    localStorage.setItem("theme", theme)
console.log(localStorage);
});

function changeTheme(NewTheme) {
 if (NewTheme === 'dark') {
    document.body.classList.add('dark-theme');
    theme = 'dark';
    themeBtn.querySelector("img").src = "./img/icons/moon.svg";
 } else {
    document.body.classList.remove('dark-theme');
    theme = 'light';
    themeBtn.querySelector("img").src = "./img/icons/sun.svg";
 }
};





