/* 
Теоретичні питання
1. Опишіть своїми словами, що таке метод об'єкту
2. Який тип даних може мати значення властивості об'єкта?
3. Об'єкт це посилальний тип даних. Що означає це поняття?

1.  Метод об'єкту це коли значення є функцією, властивість стає методом.

2.  У JavaScript значення властивості об'єкта може бути будь-яким типом даних, включаючи примітивні та об'єктні типи.

3. Об'єкти в JavaScript є посилальним типом даних. Це означає, що змінні, які мають об'єкт як значення, насправді утримують не сам об'єкт, а посилання на об'єкт в пам'яті.

Практичні завдання
1. Створіть об'єкт product з властивостями name, price та discount. Додайте метод для виведення повної ціни товару з урахуванням знижки. Викличте цей метод та результат виведіть в консоль.
2. Напишіть функцію, яка приймає об'єкт з властивостями name та age, і повертає рядок з привітанням і віком,
наприклад "Привіт, мені 30 років". Попросіть користувача ввести своє ім'я та вік
за допомогою prompt, і викличте функцію з введеними даними. Результат виклику функції виведіть з допомогою alert.
 */

/* const product = {
    name: 'apple',
    price: 15,
    discount: 5,
    fullPrice()  {
        const discountFromPrice = this.price - (this.price / 100 * this.discount);
        console.log(`Ваш това ${this.name} ціна з урахуванням знижки ${discountFromPrice}`);
    }
}

product.fullPrice() */

/* const creatGreeting= (obj) => {
    const greeting = "Привіт, мене звати ";
    greeting += obj.age + " років"
} */


// 2 Завдання =============================================================================

/* const createGreeting = (obj) =>{
     let greeting;
    if (obj.age < 10) {
        greeting = `Привіт, мене звуть ${obj.name} і мені ${obj.age} роки`
        return greeting;
    } else {
        greeting = `Привіт, мене звуть ${obj.name} і мені ${obj.age} років`
        return greeting;
    }
    
  
    
  }
  const userName = prompt("Введіть своє ім'я:");
  const userAge = prompt("Введіть свій вік:");

  const userObject = {
    name: userName,
    age: userAge
  };

  console.log(createGreeting(userObject)); */



/*  3.Опціональне. Завдання: ===========================================================================
Реалізувати повне клонування об'єкта. */


const deepClone = (obj) => {
    if (obj === null || typeof obj !== 'object') {
        return obj;
    }

    const clone = Array.isArray(obj) ? [] : {};

    for(let key in obj) {
        if(obj.hasOwnProperty(key)) {
            clone[key] = deepClone(obj[key]);
        }
    }
    return clone;
}

const originalObject ={
    name: "Alexey",
    age: 23,
    hobby: {
        dog: true,
        cats: true,
    }
};

const clonedObject = deepClone(originalObject);

clonedObject.name = "Bob";
clonedObject.hobby.dog = false;

console.log(originalObject);
console.log(clonedObject);

