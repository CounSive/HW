/* 
Теоретичні питання
1. Як можна створити рядок у JavaScript?
2. Яка різниця між одинарними (''), подвійними ("") та зворотніми (``) лапками в JavaScript?
3. Як перевірити, чи два рядки рівні між собою?
4. Що повертає Date.now()?
5. Чим відрізняється Date.now() від new Date()?

1.  В JavaScript ви можете створювати рядки, обгортаючи текст одинарними ('), подвійними (") або обертками (`).

2.  1. Одинарні та подвійні лапки:
    Різниці між одинарними та подвійними лапками в основному відсутні. Ви можете використовувати обидві для визначення рядкових значень.
    
    2.Зворотні лапки (шаблонні рядки):
    Зворотні лапки використовуються для визначення шаблонних рядків (template strings) в JavaScript.
    Шаблонні рядки дозволяють вставляти значення змінних в рядок за допомогою синтаксису ${variable}.

3. Щоб перевірити чи рівні два рядки між собою требе порівняти довжину рядка str.length
    Date.now() повертае timestamp що дорівнює кількості мілісекунд, що пройшли з 1 січня 1970 UTC +0

4. Date.now() повертає кількість мілесекунд, а new Date() повертає поточний час та дату.

1. Перевірити, чи є рядок паліндромом. Створіть функцію isPalindrome, яка приймає рядок str і повертає true, якщо рядок є паліндромом
(читається однаково зліва направо і справа наліво), або false в іншому випадку.
2. Створіть функцію, яка перевіряє довжину рядка. Вона приймає рядок, який потрібно перевірити, максимальну довжину і повертає true, якщо рядок менше або дорівнює вказаній довжині, і false, якщо рядок довший. Ця функція стане в нагоді для валідації форми. Приклади використання функції:
// Рядок коротше 20 символів
funcName('checked string', 20); // true
// Довжина рядка дорівнює 18 символів
funcName('checked string', 10); // false
3. Створіть функцію, яка визначає скільки повних років користувачу. Отримайте дату народження користувача через prompt. Функція повина повертати значення повних років на дату виклику функцію.
 */



/* const isPalindrome = (str) => {
    const originalStr = str.replace(/\s/g, '').toLowerCase();

    const reverseStr = originalStr.split('').reverse().join('');

    return originalStr === reverseStr;

    
}

const palindromStr = "А мене нема";
const notPalindromStr = "Мене звати Олексій";

console.log(isPalindrome(palindromStr));
console.log(isPalindrome(notPalindromStr)); */

// 2 Завдання ==============================================================

/* const lengthOfStr = (str, number) => {
    if (str.length <= number) {
        console.log(true);
    }else {
        console.log(false);
    }
}

lengthOfStr("hello", 3) */

// 3 Завдання ===============================================================

const userFullYear = () => {
    const dayOfBirthdInput = prompt("Введіть свою повду дату народження у форматі YYYY-MM-DD");

    const dayOfBirthd = new Date(dayOfBirthdInput);

    if(isNaN(dayOfBirthd)){
        return "Введена некоректна дата."
    }

    const currentDate = new Date();

    const ageDifferent = currentDate - dayOfBirthd;

    const userAge = Math.floor(ageDifferent / (365.25 * 24 * 60 * 60 * 1000));

    return `Вам ${userAge} повних років`
}

console.log(userFullYear());

