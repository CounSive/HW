/* 
Теоретичні питання
1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?

1.Для створення вузлів DOM існують два методи:
    ● document.createElement(tag) - Створює новий вузол елемента з
    вказаним тегом
    ● document.createTextNode(text) - Створює новий вузол тексту із заданим
    текстом

2.const navigationElement = document.querySelector('.navigation');
    navigationElement.remove();

    Або можно сробити все в одну строчку 

    document.querySelector('.navigation').remove();

3. document.body.append(div).

    Для того щоб вставити елемент перед іншим DOM-елементом
    використовуємо метод node.prepend
    Для того щоб вставити елемент після іншим DOM-елементом
    використовуємо метод node.append

Ще можемо використати один досить
універсальний метод: elem.insertAdjacentHTML(where, html).

    Щоб вставити перед елементом використовуємо beforebegin вставити html безпосередньо перед elem

    "afterend" – вставити html безпосередньо після elem.


Практичні завдання
 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
 
 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
 Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
 Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
 */

/* const footer = document.querySelector('footer');

const paragraph = footer.querySelector('p'); */

/* paragraph.insertAdjacentHTML("afterend", `<a href="#">Learn More</a>`) */

// 2 Спосіб 
/* const createLink = document.createElement('a');

createLink.innerText = "Learn More";
createLink.href ="#";

paragraph.after(createLink); */

// 2 Завдання =================================================================================

const createSelect = document.createElement("select");

createSelect.id = "rating";

const searchFeatures = document.querySelector(".features");

searchFeatures.before(createSelect);

createSelect.style.margin = "auto";
createSelect.style.display = "block";

const optionsData = [
    { value: '4', text: '4 Stars' },
    { value: '3', text: '3 Stars' },
    { value: '2', text: '2 Stars' },
    { value: '1', text: '1 Star' }
  ];

for(let i = 0; i < optionsData.length; i++) {
    let optionElement = document.createElement("option");
    optionElement.value = optionsData[i].value;
    optionElement.text = optionsData[i].text;
    createSelect.append(optionElement);
};




