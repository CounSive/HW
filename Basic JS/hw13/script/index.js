/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?
2. В чому сенс прийому делегування подій?
3. Які ви знаєте основні події документу та вікна браузера? 


1. Метод event.preventDefault() у JavaScript використовується для відміни стандартної поведінки браузера,
    яка зазвичай пов'язана з подією. Коли ви викликаєте цей метод в обробнику подій, він запобігає виконанню типових дій, 
    які зазвичай відбуваються при виникненні цієї події.

2. Прийом делегування подій є ефективним інструментом у веб-розробці для оптимізації обробки подій на сторінці. 
    Основна ідея полягає в тому, щоб не прикріплювати обробники подій безпосередньо до кожного елементу, 
    а використовувати обробник подій на батьківському елементі, який "делегує" обробку подій на дочірні елементи.

3. Основні події документу та вікна браузера включають різні типи взаємодій користувача та зміни в структурі або стані сторінки. Ось деякі з найбільш загальних подій:

    Події документу:

    DOMContentLoaded: Виникає, коли весь HTML та зовнішні ресурси були завантажені та DOM-структура документа готова, але стилі та зображення можуть ще не бути повністю завантаженими.

    load: Виникає, коли усі ресурси (включаючи стилі, зображення та інші) були повністю завантажені.

    unload: Виникає, коли користувач покидає сторінку.

    beforeunload: Виникає перед тим, як сторінка буде розтанована (unload). Дозволяє виразити попередження користувачеві про можливий втрату даних.

    resize: Виникає при зміні розміру вікна браузера.

    Події вікна браузера:

    DOMContentLoaded: Аналогічно події документу, але стосується вмісту вікна браузера.

    load: Виникає, коли весь вміст вікна (включаючи фрейми та інші вкладені ресурси) був повністю завантажений.

    unload: Виникає при закритті вікна браузера.

    beforeunload: Аналогічно події документу, виникає перед тим, як вікно буде закрите.

    resize: Виникає при зміні розміру вікна браузера.

    scroll: Виникає при прокручуванні вмісту вікна.

    focus: Виникає, коли вікно отримує фокус (становиться активним).

    blur: Виникає, коли вікно втрачає фокус.

    Це лише кілька прикладів основних подій. В більш складених веб-додатках можуть бути використані інші події для реагування на велику кількість сценаріїв взаємодії з користувачем та змін на сторінці.

Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. 
    При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. 
При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/
const tabs = document.querySelector(".tabs");

const tabsContent = document.querySelectorAll(".content");



/* tabsTitle.forEach (tab => {
    tab.addEventListener('click', () => {

    const tabId = tab.getAttribute('data-tab');

      // Приховуємо всі контенти
      tabsContent.forEach(content => {
        content.classList.remove('active');
      });

      tabsTitle.forEach(tab => {
        tab.classList.remove('active');
      });
      // Відображаємо лише потрібний контент
      const selectedContent = document.querySelector(`.content[data-tab-content="${tabId}"]`);
      selectedContent.classList.add('active');
      tab.classList.add('active');
    });
}); */


tabs.addEventListener('click', (event) => {
    if (event.target.classList.contains('tabs-title')) {
        const selectedTab = event.target;
        const tabId = selectedTab.getAttribute('data-tab');

        tabs.querySelectorAll('.tabs-title').forEach(tab => {
            tab.classList.remove('active');
        });
        selectedTab.classList.add('active');

        tabsContent.forEach(content => {
            content.classList.remove('active');
        });

        const selectedContent = document.querySelector(`.content[data-tab-content="${tabId}"]`);
        selectedContent.classList.add('active');
        tabs.classList.add('active');
    }
})