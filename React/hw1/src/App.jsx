import { useState } from "react";

// MODAL IMPORTS
import ModalWrapper from "./components/Modal/ModalWrapper";
import Modal from "./components/Modal/Modal";
import ModalHeader from "./components/Modal/ModalHeader";
import ModalClose from "./components/Modal/ModalClose";
import ModalBody from "./components/Modal/ModalBody";
import ModalFooter from "./components/Modal/ModalFooter";

import Button from "./components/Button";

import "./App.sass";

function App() {
  const [showModal1, setShowModal1] = useState(false);
  const [showModal2, setShowModal2] = useState(false);


  const handleOpenModal1 = () => setShowModal1((m) => !m);
  const handleOpenModal2 = () => setShowModal2((m) => !m);


  return (
    <div className="app">
     <Button classNames="btn" onClick={handleOpenModal1}>
        Open first modal
      </Button>
      <Button classNames="btn" onClick={handleOpenModal2}>
        Open second modal
      </Button>


      {showModal1 && (
        <ModalWrapper onShowModal={handleOpenModal1}>
          <Modal>
            <ModalHeader>
              <div>
                <div
                  style={{ width: "276px", height: "140px", margin: "0 auto" }}
                  className="img"
                >
                  <img
                    style={{ width: "100%", height: "100%" }}
                    src="../public/dog.jpg"
                    alt="dog"
                  />
                </div>
                <h1 style={{ marginTop: "85px" }}>Product delete</h1>
              </div>
            </ModalHeader>
            <ModalClose onClick={handleOpenModal1} />
            <ModalBody>
              By clicking the “Yes, Delete” button, PRODUCT NAME will be
              deleted.
            </ModalBody>
            <ModalFooter
              firstText="No, cancel"
              secondaryText="Yes, Delete"
              firstClick={() => {
                setShowModal1(false);
              }}
              secondaryClick={() => {
                setShowModal1(false);
              }}
            />
          </Modal>
        </ModalWrapper>
      )}


      {showModal2 && (
        <ModalWrapper onShowModal={handleOpenModal2}>
          <Modal>
            <ModalHeader>
              <h1>Add Product “NAME”</h1>
            </ModalHeader>
            <ModalClose onClick={handleOpenModal2} />
            <ModalBody>Description for you product</ModalBody>
            <ModalFooter
              firstText="ADD TO FAVORITE"
              firstClick={() => {
                setShowModal2(false);
              }}
            />
          </Modal>
        </ModalWrapper>
      )}
    </div>
  );
}

export default App;
