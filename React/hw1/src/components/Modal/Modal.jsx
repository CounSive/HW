import PropTypes from "prop-types";

function Modal({ children }) {
  return <div className="modal">{children}</div>;
}

Modal.propTypes = {
  children: PropTypes.node,
};

export default Modal;
