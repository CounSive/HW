import PropTypes from "prop-types";

function ModalWrapper({ children, onShowModal }) {
  return (
    <div
      className="modal-wrapper"
      onClick={(e) => {
        if (e.target.classList.contains("modal-wrapper")) onShowModal();
      }}
    >
      {children}
    </div>
  );
}

ModalWrapper.propTypes = {
  children: PropTypes.node,
  onShowModal: PropTypes.func,
};

export default ModalWrapper;
