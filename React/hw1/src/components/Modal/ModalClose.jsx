import PropTypes from "prop-types";

function ModalClose({ onClick }) {
  return <div className="modal__close" onClick={onClick}></div>;
}

ModalClose.propTypes = {
  onClick: PropTypes.func,
};

export default ModalClose;
