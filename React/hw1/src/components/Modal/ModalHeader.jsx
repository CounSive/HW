import PropTypes from "prop-types";

function ModalHeader({ children }) {
  return <div className="modal__header">{children}</div>;
}

ModalHeader.propTypes = {
  children: PropTypes.node,
};

export default ModalHeader;
