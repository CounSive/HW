import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  data: [],
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    getAllCartItems(state) {
      state.data = localStorage.getItem("addedToCard")
        ? JSON.parse(localStorage.getItem("addedToCard"))
        : [];

      localStorage.setItem("addedToCard", JSON.stringify(state.data));
    },
    addToCart(state, action) {
      if (!state.data.find((elem) => elem.id === action.payload.id)) {
        state.data.push({ ...action.payload, count: 1 });
        localStorage.setItem("addedToCard", JSON.stringify(state.data));
      }
    },
    removeFromCart(state, action) {
      const index = state.data.findIndex((elem) => elem.id === action.payload);
      if (index !== -1) state.data.splice(index, 1);
      localStorage.setItem("addedToCard", JSON.stringify(state.data));
    },
    increaseCount(state, action) {
      const index = state.data.findIndex((todo) => todo.id === action.payload);
      if (index !== -1) ++state.data[index].count;
      localStorage.setItem("addedToCard", JSON.stringify(state.data));
    },
    decreaseCount(state, action) {
      const index = state.data.findIndex((todo) => todo.id === action.payload);
      if (state.data[index].count <= 1) return;
      if (index !== -1) --state.data[index].count;
    },
    removeAllItems(state) {
      state.data = [];
      localStorage.setItem("addedToCard", []);
    },

  },
});

export const {
  getAllCartItems,
  addToCart,
  removeFromCart,
  increaseCount,
  decreaseCount,
  removeAllItems
} = cartSlice.actions;

export default cartSlice.reducer;
