import cartSlice from "./cartSlice";
import { expect } from "@jest/globals";

describe("cartReducer works", () => {
    test("should get all cart items", () => {
        const state = {
            data: [],
        };
        const action = { type: "cart/getAllCartItems" };
        expect(cartSlice(state, action)).toEqual({ data: [] });
    });

    test("should add cart item", () => {
        const state = {
            data: [],
        };
        const item = {
            productName: "Arcane - Rubick",
            price: "1250",
            src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
            id: 4,
            style: "Second",
            count: 1,
        };
        const action = { type: "cart/addToCart", payload: item };
        expect(cartSlice(state, action)).toEqual({
            data: [
                {
                    productName: "Arcane - Rubick",
                    price: "1250",
                    src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
                    id: 4,
                    style: "Second",
                    count: 1,
                },
            ],
        });
    });

    test("should remove cart item", () => {
        const state = {
            data: [
                {
                    productName: "Arcane - Rubick",
                    price: "1250",
                    src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
                    id: 4,
                    style: "Second",
                    count: 1,
                },
            ],
        };
        const action = { type: "cart/removeFromCart", payload: 4 };
        expect(cartSlice(state, action)).toEqual({
            data: [],
        });
    });

    test("should increase count", () => {
        const state = {
            data: [
                {
                    productName: "Arcane - Rubick",
                    price: "1250",
                    src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
                    id: 4,
                    style: "Second",
                    count: 1,
                },
            ],
        };
        const action = { type: "cart/increaseCount", payload: 4 };
        expect(cartSlice(state, action)).toEqual({
            data: [
                {
                    productName: "Arcane - Rubick",
                    price: "1250",
                    src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
                    id: 4,
                    style: "Second",
                    count: 2,
                },
            ],
        });
    });

    test("should decrease count", () => {
        const state = {
            data: [
                {
                    productName: "Arcane - Rubick",
                    price: "1250",
                    src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
                    id: 4,
                    style: "Second",
                    count: 2,
                },
            ],
        };
        const action = { type: "cart/decreaseCount", payload: 4 };
        expect(cartSlice(state, action)).toEqual({
            data: [
                {
                    productName: "Arcane - Rubick",
                    price: "1250",
                    src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
                    id: 4,
                    style: "Second",
                    count: 1,
                },
            ],
        });
    });
    test("should remove all items", () => {
        const state = {
            data: [
                {
                    productName: "Arcane - Rubick",
                    price: "1250",
                    src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
                    id: 4,
                    style: "Second",
                    count: 2,
                },
            ],
        };
        const action = { type: "cart/removeAllItems" };
        expect(cartSlice(state, action)).toEqual({
            data: [],
        });
    });
});
