import favouritesSlice from "./favouritesSlice";
import { expect } from "@jest/globals";

describe("favouriteReducer works", () => {
  test("should get all favourite items", () => {
    const state = {
      data: [],
    };
    const action = { type: "favourites/getAllFavourites" };
    expect(favouritesSlice(state, action)).toEqual({ data: [] });
  });

  test("should add favourite item", () => {
    const state = {
      data: [],
    };
    const item = {
      productName: "Arcane - Rubick",
      price: "1250",
      src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
      id: 4,
      style: "Second",
    };
    const action = { type: "favourites/addToFavourites", payload: item };
    expect(favouritesSlice(state, action)).toEqual({
      data: [
        {
            productName: "Arcane - Rubick",
            price: "1250",
            src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
            id: 4,
            style: "Second",
        },
      ],
    });
  });

  test("should remove favourite item", () => {
    const state = {
      data: [
        {
            productName: "Arcane - Rubick",
            price: "1250",
            src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
            id: 4,
            style: "Second",
        },
      ],
    };
    const action = { type: "favourites/removeFromFavourites", payload: 4 };
    expect(favouritesSlice(state, action)).toEqual({
      data: [],
    });
  });
});
