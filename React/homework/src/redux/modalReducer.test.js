import modalSlice from "./modalSlice";
import { expect } from "@jest/globals";

describe("modalReducer works", () => {
  test("should modalReducerApp open modal", () => {
    const state = {
      isOpenAdd: false,
    };
    const action = { type: "modal/toggleModalAdd" };
    expect(modalSlice(state, action)).toEqual({ isOpenAdd: true });
  });

  test("should modalReducerApp close modal", () => {
    const state = {
      isOpenAdd: true,
    };
    const action = { type: "modal/toggleModalAdd" };
    expect(modalSlice(state, action)).toEqual({ isOpenAdd: false });
  });

  test("should modalReducerRemove open modal", () => {
    const state = {
      isOpenRemove: false,
    };
    const action = { type: "modal/toggleModalRemove" };
    expect(modalSlice(state, action)).toEqual({ isOpenRemove: true });
  });

  test("should modalReducerRemove close modal", () => {
    const state = {
      isOpenRemove: true,
    };
    const action = { type: "modal/toggleModalRemove" };
    expect(modalSlice(state, action)).toEqual({ isOpenRemove: false });
  });

  test("should configModal config the modal", () => {
    const state = {
      name: "",
      price: 0,
      style: "",
      src: "",
      id: "",
    };
    const action = {
      type: "modal/configModal",
      payload: {
        name: "test_name",
        price: 57,
        style: "First",
        src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRpSALEDhjBgTvZsse8U0HjVwX9ZfKF7gutvv_a7ixBgg&s",
        id: "5",
      },
    };
    expect(modalSlice(state, action)).toEqual({
      name: "test_name",
      price: 57,
      style: "First",
      src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRpSALEDhjBgTvZsse8U0HjVwX9ZfKF7gutvv_a7ixBgg&s",
      id: "5",
    });
  });
});
