import s from "./Cart.module.sass";

import PropTypes from "prop-types";

("../components/CartItem/CartItem");
import FavouriteItem from "../components/FavouriteItem/FavouriteItem";
import { useSelector } from "react-redux";
import { Link, NavLink } from "react-router-dom";
import AddToCard from "../components/Modal/AddToCard";


function Favourites() {
  const favouriteProducts = useSelector((store) => store.favourites.data);

  return (
    <div className={s.container}>
      <div className={s.path}>
        <Link to="/">Home</Link>
        <span>{">"}</span>
        <NavLink
          to="/favourites"
          className={({ isActive }) => isActive && s.active}
        >
          Favourites
        </NavLink>
      </div>

      <h2>Favourites</h2>
      <div className={s.carts}>
        {favouriteProducts.map((product) => (
          <FavouriteItem key={product.id} product={product} />
        ))}
      </div>
      <AddToCard />
    </div>
  );
}

Favourites.propTypes = {
  favouriteProducts: PropTypes.array,
  onDeleteFromFavourite: PropTypes.func,
};

export default Favourites;
