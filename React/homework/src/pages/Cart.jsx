import s from "./Cart.module.sass";

import PropTypes from "prop-types";
import DeleteProduct from "../components/Modal/DeleteProduct";
import CheckOutForm from "../components/CheckOutForm/CheckOutForm";
import OrderSummary from "../components/OrderSummary/OrderSummary";
import { Link, NavLink } from "react-router-dom";

function Cart() {
  return (
    <div className={s.container}>
     <div className={s.path}>
        <Link to="/">Home</Link>
        <span>{">"}</span>
        <NavLink to="/cart" className={({ isActive }) => isActive && s.active}>
          Check Out
        </NavLink>
      </div>
      <h2>Check Out</h2>
      <div className={s.sections}>
        <CheckOutForm />
        <OrderSummary />
      </div>
      <DeleteProduct />
    </div>
  );
}

Cart.propTypes = {
  addedToCard: PropTypes.array,
  setAddedToCard: PropTypes.func,
  onDeleteFromCart: PropTypes.func,
};

export default Cart;
