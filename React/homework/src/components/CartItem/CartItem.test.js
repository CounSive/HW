import CartItem from "./CartItem";
import { render } from "@testing-library/react";
import { expect } from "@jest/globals";
import store from "../../store";
import { Provider } from "react-redux";

describe("CartItem snapchot testing", () => {
  test("should render CartItem", () => {
    const product = {
      productName: "Arcane - Rubick",
      price: "1250",
      src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
      id: 4,
      style: "Second",
      count: 3,
    };
    const { asFragment } = render(
      <Provider store={store}>
        <CartItem product={product} />
      </Provider>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
