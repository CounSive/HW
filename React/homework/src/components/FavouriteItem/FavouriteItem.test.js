import FavouriteItem from "./FavouriteItem";
import { render } from "@testing-library/react";
import { expect } from "@jest/globals";
import store from "../../store";
import { Provider } from "react-redux";

describe("FavouriteItem snapchot testing", () => {
  test("should render FavouriteItem", () => {
    const product = {
      productName: "Arcane - Rubick",
      price: "1250",
      src: "https://prodota.ru/uploads/news/imperavi/1545261767549.png",
      id: 4,
      style: "Second",
    };
    const { asFragment } = render(
      <Provider store={store}>
        <FavouriteItem product={product} />
      </Provider>
    );
    expect(asFragment()).toMatchSnapshot();
  });
});
