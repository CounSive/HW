import PropTypes from "prop-types";

import { close } from "./Modal.module.sass";

function ModalClose({ onClick }) {
  return <div className={close} onClick={onClick}></div>;
}

ModalClose.propTypes = {
  onClick: PropTypes.func,
};

export default ModalClose;
