import PropTypes from "prop-types";

import { header } from "./Modal.module.sass";

function ModalHeader({ children }) {
  return <div className={header}>{children}</div>;
}

ModalHeader.propTypes = {
  children: PropTypes.node,
};

export default ModalHeader;
