import PropTypes from "prop-types";

import { modal } from "./Modal.module.sass";

function Modal({ children }) {
  return <div className={modal}>{children}</div>;
}

Modal.propTypes = {
  children: PropTypes.node,
};

export default Modal;
