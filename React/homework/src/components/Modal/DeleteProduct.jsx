import ModalWrapper from "./ModalWrapper";
import Modal from "./Modal";
import ModalHeader from "./ModalHeader";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";

import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { toggleModalRemove } from "../../redux/modalSlice";
import { removeFromCart } from "../../redux/cartSlice";

function DeleteProduct() {
  const {
    isOpenRemove: isOpen,
    name,
    id,
    src,
  } = useSelector((state) => state.modal);

  const dispatch = useDispatch();

  function handleToggleModal() {
    dispatch(toggleModalRemove());
  }

  function handleDeleteFromCart() {
    dispatch(removeFromCart(id));
    dispatch(toggleModalRemove());
  }

  if (!isOpen) return null;

  return (
    <ModalWrapper onShowModal={handleToggleModal}>
      <Modal>
        <ModalHeader>
          <div>
            <div
              style={{ width: 300, height: 300, margin: "0 auto" }}
              className="img"
            >
              <img
                style={{ width: "100%", height: "100%" }}
                src={src}
                alt="bg"
              />
            </div>
            <h1 style={{ marginTop: "85px" }}>Product delete</h1>
          </div>
        </ModalHeader>
        <ModalClose onClick={handleToggleModal} />
        <ModalBody>
          By clicking the “Yes, Delete” button, {name} will be deleted.
        </ModalBody>
        <ModalFooter
          firstText="No, cancel"
          secondaryText="Yes, Delete"
          firstClick={handleToggleModal}
          secondaryClick={handleDeleteFromCart}
        />
      </Modal>
    </ModalWrapper>
  );
}

DeleteProduct.propTypes = {
  handleOpenModal1: PropTypes.func,
  onShowModal: PropTypes.func,
  onDeleteFromCart: PropTypes.func,
  productName: PropTypes.string,
  img: PropTypes.string,
  id: PropTypes.number,
};

export default DeleteProduct;
