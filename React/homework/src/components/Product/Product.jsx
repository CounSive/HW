import PropTypes from "prop-types";
import s from "./Product.module.sass";
import { useDispatch } from "react-redux";
import {
  addToFavourites,
  removeFromFavourites,
} from "../../redux/favouritesSlice";
import { configModal, toggleModalAdd } from "../../redux/modalSlice";

function Product({ product }) {
  const { productName: name, src, price, style, id, isFavourite } = product;
  const dispatch = useDispatch();

  function handleFavourites() {
    if (isFavourite) {
      dispatch(removeFromFavourites(id));
    } else {
      dispatch(addToFavourites(product));
    }
  }

  function showModal() {
    dispatch(toggleModalAdd());
    dispatch(configModal({ name, price, style, src, id }));
  }

  return (
    <>
      <div className={s.container}>
        <div className={s.img}>
          <img src={src} alt={name} />
        </div>
        <div className={s.info}>
          <div className={s.name}>{name}</div>
          <div className={s.price}>{price} UAH</div>
          <div className={s.style}>{style}</div>
          <div className={s.btns}>
            <button onClick={showModal}>Add to cart</button>
            <div
              className={`${s.favourite} ${isFavourite && s.favouriteActive}`}
              onClick={handleFavourites}
            >
              <svg
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
              >
                <path d="M12,17.27L18.18,21L16.54,13.97L22,9.24L14.81,8.62L12,2L9.19,8.62L2,9.24L7.45,13.97L5.82,21L12,17.27Z" />
              </svg>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

Product.propTypes = {
  product: PropTypes.object,
  onAddtoFavourite: PropTypes.func,
  onDeleteFromFavourite: PropTypes.func,
  favouriteProducts: PropTypes.array,
  onAddToCard: PropTypes.func,
};

export default Product;
